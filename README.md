# OpenML dataset: debutanizer

https://www.openml.org/d/23516

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

&quot;The debutanizer column is part of a desulfuring and naphtha splitter plant.&quot;

u1 Top temperature
u2 Top pressure
u3 Reflux flow
u4 Flow to next process
u5 6th tray temperature
u6 Bottom temperature
u7 Bottom temperature
y Butane content in the debutanizer bottoms (Naphtha splitter feed)

See Appendix A.3 of Fortuna, L., Graziani, S., Rizzo, A., Xibilia, M.G. &quot;Fortuna, L., Graziani, S., Rizzo, A., Xibilia, M.G.&quot; (Springer 2007) for more info.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/23516) of an [OpenML dataset](https://www.openml.org/d/23516). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/23516/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/23516/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/23516/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

